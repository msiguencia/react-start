var React = require('react');

var Battle = React.createClass({
    render: function () {
        return this.props.isLoading == true ?
            <h1>Loading</h1> : <div>
                <h1>Battle!</h1> 
                <p>{JSON.stringify(this.props.playerInfo)}</p>
            </div>
    }
});

module.exports = Battle;
