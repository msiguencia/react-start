import React from 'react'
import {Link} from 'react-router'

/*var Home = React.createClass({

    render: function () {
        return (<p>Hello</p>);
    }
});

module.exports = Home;*/

export default React.createClass({
    render() {
        return (
                <div className="jumbotron col-sm-12 text-center">
                    <h1>GitHub Battle</h1>
                    <p className="lead">Some Words</p>
                    {/*<Link to='/parent'> Parent </Link>*/}
                    <Link to='/playerOne'>
                        <button type="button" className="btn btn-lrg btn-success">Get Started</button>
                    </Link>
                </div>
        )
    }
})
