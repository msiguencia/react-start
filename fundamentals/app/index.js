import React from 'react'
import {render} from 'react-dom'
import {Router, Route, hashHistory, IndexRoute} from 'react-router'

import Home from '../components/Home'
import PromptContainer from '../containers/PromptContainer'
import ParentComponent from '../components/ParentComponent'
import BattleContainer from '../containers/BattleContainer'

render((
<Router history={hashHistory}>
    <Route path='/' component={Home}/>
    {/*<Route path='/parent' component={ParentComponent} />*/}
    <Route path='/playerOne' header='Player One' component={PromptContainer}/>
    <Route path='/playerTwo/:playerOne' header='Player Two' component={PromptContainer}/>
    <Route path='/battle' component={BattleContainer}/>
</Router>
), document.getElementById('app'))
