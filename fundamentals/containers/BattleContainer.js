import React from 'react';
import githubHelper from '../helpers/githubHelpers';

var Battle = require('../components/Battle.js');

var BattleContainer = React.createClass({
    getInitialState: function () {
        return {
            isLoading: true,
            playerInfo: []
        };
    },
    componentDidMount: function () {
        var query = this.props.location.query;
        githubHelper.getPlayersInfo([query.playerOne,query.playerTwo]).then(
            function (players){
                this.setState({
                    isLoading: false,
                    playerInfo: [players[0],players[1]]
                });
            }.bind(this));
    },
    render: function () {
        return <Battle isLoading={this.state.isLoading}
                       playerInfo={this.state.playerInfo}/>
    }
});

module.exports = BattleContainer;
