var React = require('react');
import Prompt from '../components/Prompt.js'

var PromptContainer = React.createClass({
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    getInitialState () {
        return {
            username: ''
        }
    },
    updateUserHandler (event) {
        this.setState({
            username: event.target.value
        })
    },
    submitUserHandler (event) {
        //prevents a form submission, default behavior of a form
        event.preventDefault();
        //temporarily caching the username before reset
        var username = this.state.username;
        //reset state so the input box is cleared (input uses value to fill in
        //the box initially)
        this.setState({
            username: ''
        });
        //At player two
        if (this.props.params.playerOne) {
            this.context.router.push({
                pathname: '/battle',
                query: {
                    playerOne: this.props.params.playerOne,
                    playerTwo: this.state.username
                }
            })
        }
        else {
            //At the player one
            this.context.router.push('/playerTwo/' + this.state.username)
        }
    },
    render () {
        return (
                <Prompt onSubmitUser={this.submitUserHandler}
                        onUpdateUser={this.updateUserHandler}
                        header={this.props.route.header}
                        username={this.state.username}/>
        )
    }
});

module.exports = PromptContainer;
